module gitlab.com/shadowy/sei/notifications/api

go 1.16

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/rs/zerolog v1.22.0
	github.com/streadway/amqp v0.0.0-20190404075320-75d898a42a94 // indirect
	gitlab.com/shadowy/go/rabbitmq/v2 v2.0.1
	gitlab.com/shadowy/sei/notifications/go-notification-proto v0.6.0
	google.golang.org/protobuf v1.26.0
)
