package api

import (
	"encoding/json"
	"github.com/rs/zerolog/log"
	"github.com/streadway/amqp"
	"gitlab.com/shadowy/go/rabbitmq/v2"
	"gitlab.com/shadowy/sei/notifications/go-notification-proto/bus"
	"google.golang.org/protobuf/proto"
)

type NotificationService struct {
	output *rabbitmq.Output
}

func CreateNotificationService(queue *rabbitmq.RabbitMq, out string) (*NotificationService, error) {
	log.Logger.Debug().Str("out", out).Msg("CreateNotificationService")
	output, err := queue.GetOutputForDestination(out)
	if err != nil {
		log.Logger.Error().Err(err).Stack().Msg("CreateNotificationService")
	}
	res := new(NotificationService)
	res.output = output
	return res, nil
}

func (srv *NotificationService) Send(clientID, templateCode string, data interface{}) error {
	l := log.With().Str("templateCode", templateCode).Str("clientID", clientID).Logger()
	l.Debug().Msg("NotificationService.Send")
	d, err := json.Marshal(data)
	if err != nil {
		l.Error().Err(err).Stack().Msg("NotificationService.Send json.Marshal")
		return err
	}
	msg := bus.Message{
		ClientID:     clientID,
		TemplateCode: templateCode,
		Data:         string(d),
	}
	out, err := proto.Marshal(&msg)
	if err != nil {
		l.Error().Err(err).Stack().Msg("NotificationService.Send proto.Marshal")
		return err
	}
	header := amqp.Table{}
	header[HeaderType] = "document"
	header[HeaderDocumentType] = "message"
	header[HeaderAction] = "add"
	err = srv.output.SendWithHeader(out, header, 1)
	if err != nil {
		l.Error().Err(err).Stack().Msg("NotificationService.Send send")
		return err
	}
	return nil
}
