# CHANGELOG

<!--- next entry here -->

## 0.3.1
2022-08-31

### Fixes

- change logics for getting out exchange (5517e3230e43ab96ec0d9715a46c1bb5cb0d08ab)
- change logics for getting out exchange (ed8a0d17a64f9831fb2f99909265f798ca6bd061)

## 0.3.0
2021-06-16

### Features

- add possibility to select output stream (295ad69da65aca80771e9a5fe1491c3146544194)

## 0.2.2
2021-06-07

### Fixes

- add header for queue messages (2432e7ea63b1d5fb820e001ae2b817798135f56e)

## 0.2.1
2021-06-07

### Fixes

- change service name UserService to ClientService (34a8afe146b137a4a9835d76b3df4cc41d406a1e)

## 0.2.0
2021-06-07

### Features

- add UserService (cf1f436c26aaca4e7eb9c64057c17141d069ce39)

## 0.1.0
2021-05-31

### Features

- initial commit (dd8ae6bc011d85c2ec3f48a31a1945f96dac0f43)

