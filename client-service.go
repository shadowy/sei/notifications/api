package api

import (
	"github.com/rs/zerolog/log"
	"github.com/streadway/amqp"
	"gitlab.com/shadowy/go/rabbitmq/v2"
	"gitlab.com/shadowy/sei/notifications/go-notification-proto/bus"
	"google.golang.org/protobuf/proto"
)

type ClientService struct {
	output *rabbitmq.Output
}

func CreateClientService(queue *rabbitmq.RabbitMq, out string) (*ClientService, error) {
	log.Logger.Debug().Str("out", out).Msg("CreateUserService")
	output, err := queue.GetOutput(out)
	if err != nil {
		log.Logger.Error().Err(err).Stack().Msg("CreateUserService")
	}
	res := new(ClientService)
	res.output = output
	return res, nil
}

func (srv *ClientService) Send(clientID, lastName, firstName, patronymicName, email, phone string) error {
	l := log.With().
		Str("clientID", clientID).
		Str("lastName", lastName).
		Str("firstName", firstName).
		Str("patronymicName", patronymicName).
		Str("email", email).
		Str("phone", phone).
		Logger()
	l.Debug().Msg("ClientService.Send")
	msg := bus.Client{
		ClientID:       clientID,
		LastName:       lastName,
		FirstName:      firstName,
		PatronymicName: patronymicName,
		Email:          email,
		Phone:          phone,
	}
	out, err := proto.Marshal(&msg)
	if err != nil {
		l.Error().Err(err).Stack().Msg("ClientService.Send proto.Marshal")
		return err
	}
	header := amqp.Table{}
	header[HeaderType] = "document"
	header[HeaderDocumentType] = "client"
	header[HeaderAction] = "set"
	err = srv.output.SendWithHeader(out, header, 1)
	if err != nil {
		l.Error().Err(err).Stack().Msg("ClientService.Send send")
		return err
	}
	return nil
}
